package rekt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/mholt/caddy/caddyhttp/httpserver"
)

type (
	Handler struct {
		httpserver.Handler
		endpoint string
		client   *http.Client
		ticker   *time.Ticker
		id       int
		counter  uint64
		DashMap  *DashMap
	}
	DashMap struct {
		sync.RWMutex
		Handler *Handler
		Values  map[string]*DashEntry
	}
	DashEntry struct {
		Key    string
		Map    *DashMap
		Update chan bool
	}
)

func (d *DashMap) New(key string) {
	d.Lock()
	defer d.Unlock()
	e := &DashEntry{
		Key:    key,
		Update: make(chan bool),
		Map:    d,
	}
	go e.CloseLoop()
	d.Values[key] = e
	atomic.AddUint64(&d.Handler.counter, 1)
	fmt.Printf("New DASH Connection from %s, Active: %d\n", key, atomic.LoadUint64(&d.Handler.counter))
}
func (d *DashMap) Get(key string) *DashEntry {
	d.RLock()
	defer d.RUnlock()
	return d.Values[key]
}
func (d *DashMap) Close(key string) {
	d.Lock()
	defer d.Unlock()
	delete(d.Values, key)
	atomic.AddUint64(&d.Handler.counter, ^uint64(0))
	fmt.Printf("DASH Connection Ended from %s, Active: %d\n", key, atomic.LoadUint64(&d.Handler.counter))
}

func (e *DashEntry) CloseLoop() {
	defer e.Map.Close(e.Key)
	for {
		select {
		case <-e.Update:
		case <-time.After(10 * time.Second):
			return
		}
	}
}

func (h *Handler) ServeHTTP(res http.ResponseWriter, req *http.Request) (int, error) {
	if strings.HasPrefix(req.URL.Path, "/dash") {
		go func() {
			e := h.DashMap.Get(req.RemoteAddr)
			if e != nil {
				e.Update <- true
				return
			}
			h.DashMap.New(req.RemoteAddr)
		}()
		return h.Handler.ServeHTTP(res, req)
	}
	atomic.AddUint64(&h.counter, 1)
	fmt.Printf("New Connection from %s, Active: %d\n", req.RemoteAddr, atomic.LoadUint64(&h.counter))
	code, err := h.Handler.ServeHTTP(res, req)
	atomic.AddUint64(&h.counter, ^uint64(0))
	fmt.Printf("Connection Ended from %s, Active: %d\n", req.RemoteAddr, atomic.LoadUint64(&h.counter))
	return code, err
}

func (h *Handler) pushloop() {
	out := Output{ID: h.id}
	for range time.NewTicker(delay * time.Second).C {
		out.Counter = atomic.LoadUint64(&h.counter)
		data, err := json.Marshal(out)
		if err != nil {
			continue
		}
		req, err := http.NewRequest("POST", h.endpoint, bytes.NewBuffer(data))
		if err != nil {
			continue
		}
		res, err := h.client.Do(req)
		if err != nil {
			continue
		}

		res.Body.Close() // We don't care about response
	}
}
