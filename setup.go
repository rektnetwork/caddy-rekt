package rekt

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

const delay = 5

func Setup(c *caddy.Controller) error {
	var h *Handler
	for c.Next() {
		if h != nil {
			return fmt.Errorf("rekt cannot be defined more than once")
		}
		h = &Handler{client: &http.Client{Timeout: delay * time.Second}}

		if !c.NextArg() {
			return c.ArgErr()
		}
		id, err := strconv.Atoi(c.Val())
		if err != nil {
			return c.ArgErr()
		}
		h.id = id

		if !c.NextArg() {
			return c.ArgErr()
		}
		h.endpoint = c.Val()
	}
	h.DashMap = &DashMap{
		Values:  make(map[string]*DashEntry),
		Handler: h,
	}
	go h.pushloop()

	cfg := httpserver.GetConfig(c)

	handler := func(next httpserver.Handler) httpserver.Handler {
		h.Handler = next
		return h
	}
	cfg.AddMiddleware(handler)

	return nil
}

func init() {
	caddy.RegisterPlugin("rekt", caddy.Plugin{
		ServerType: "http",
		Action:     Setup,
	})
	httpserver.RegisterDevDirective("rekt", "log")
}
