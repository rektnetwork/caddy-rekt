package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"sync/atomic"
	"time"
)

type (
	Update struct {
		ID      int    `json:"id"`
		Counter uint64 `json:"counter"`
	}
)

var listeners [5]uint64
var Total, HourlyMax, DailyMax uint64
var History = make([]uint64, 24, 24)

func UpdateListeners(id int, count uint64) {
	var total uint64
	for i := range listeners {
		if i == id {
			total += count
			atomic.StoreUint64(&listeners[i], count)
			continue
		}
		total += atomic.LoadUint64(&listeners[i])
	}
	atomic.StoreUint64(&Total, total)
	if total > atomic.LoadUint64(&HourlyMax) {
		atomic.StoreUint64(&HourlyMax, total)
		if total > atomic.LoadUint64(&DailyMax) {
			atomic.StoreUint64(&DailyMax, total)
		}
	}
}

func CheckDailyMax() {
	var daily uint64
	var current uint64
	for i := range History {
		current = atomic.LoadUint64(&History[i])
		if current > daily {
			daily = current
		}
	}
	atomic.StoreUint64(&DailyMax, daily)
}

func HourlyMaxTicker() {
	ticker := time.NewTicker(time.Hour)
	for range ticker.C {
		for i := range History {
			if i == 23 {
				atomic.StoreUint64(&History[i], atomic.LoadUint64(&HourlyMax))
				atomic.StoreUint64(&HourlyMax, atomic.LoadUint64(&Total))
				break
			}
			atomic.StoreUint64(&History[i], atomic.LoadUint64(&History[i+1]))
		}
		CheckDailyMax()
	}
}

func HandleListeners(res http.ResponseWriter, req *http.Request) {
	var update Update
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Error Reading Body %s", err)
		return
	}
	err = json.Unmarshal(body, &update)
	if err != nil {
		log.Printf("Error Unmarshalling Body %s", err)
	}
	UpdateListeners(update.ID, update.Counter)
}

func main() {
	http.HandleFunc("/", HandleListeners)
	log.Fatal(http.ListenAndServe(":50000", nil))
}
