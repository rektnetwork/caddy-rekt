package rekt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"sync/atomic"
	"time"

	"github.com/mholt/caddy"
)

type (
	Listener struct {
		caddy.Listener
		counter  uint64
		id       int
		endpoint string
		client   *http.Client
		excludes []*net.IPNet
	}
	Connection struct {
		net.Conn
		closed chan bool
	}
	Output struct {
		ID      int    `json:"id"`
		Counter uint64 `json:"counter"`
	}
)

func (l *Listener) Accept() (net.Conn, error) {
	c, err := l.Listener.Accept()
	if err != nil {
		return c, err
	}
	// Parse excluded listeners
	addr, _, err := net.SplitHostPort(c.RemoteAddr().String())
	if err != nil {
		return c, err
	}
	ip := net.ParseIP(addr)
	for _, v := range l.excludes {
		if v.Contains(ip) {
			return c, err
		}
	}
	atomic.AddUint64(&l.counter, 1)
	conn := &Connection{Conn: c, closed: make(chan bool)}
	fmt.Printf("New Connection from %s: Active %d\n", conn.RemoteAddr(), atomic.LoadUint64(&l.counter))
	go func() {
		select {
		case <-conn.closed:
		case <-time.After(4 * time.Hour):
		}
		atomic.AddUint64(&l.counter, ^uint64(0))
		fmt.Printf("Closed Connection from %s: Active %d\n", conn.RemoteAddr(), atomic.LoadUint64(&l.counter))
	}()
	return conn, err
}

func (l *Listener) pushloop() {
	out := Output{ID: l.id}
	for range time.NewTicker(delay * time.Second).C {
		out.Counter = atomic.LoadUint64(&l.counter)
		data, err := json.Marshal(out)
		if err != nil {
			continue
		}
		req, err := http.NewRequest("POST", l.endpoint, bytes.NewBuffer(data))
		if err != nil {
			continue
		}
		res, err := l.client.Do(req)
		if err != nil {
			continue
		}
		res.Body.Close() // We don't care about response
	}
}

func (c *Connection) Close() error {
	select {
	case c.closed <- true:
	default:
	}
	// Call parent close
	return c.Conn.Close()
}
